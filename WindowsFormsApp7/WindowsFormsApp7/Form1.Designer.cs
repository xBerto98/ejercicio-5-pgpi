﻿namespace WindowsFormsApp7
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.boton_sistema = new System.Windows.Forms.Button();
            this.ventana = new System.Windows.Forms.RichTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.boton_RAM = new System.Windows.Forms.Button();
            this.boton_disco = new System.Windows.Forms.Button();
            this.boton_GPU = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // boton_sistema
            // 
            this.boton_sistema.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.boton_sistema.Location = new System.Drawing.Point(36, 27);
            this.boton_sistema.Margin = new System.Windows.Forms.Padding(4);
            this.boton_sistema.Name = "boton_sistema";
            this.boton_sistema.Size = new System.Drawing.Size(156, 43);
            this.boton_sistema.TabIndex = 0;
            this.boton_sistema.Text = "Caracteristicas del sistema";
            this.boton_sistema.UseVisualStyleBackColor = false;
            this.boton_sistema.Click += new System.EventHandler(this.boton_sistema_Click);
            // 
            // ventana
            // 
            this.ventana.Location = new System.Drawing.Point(220, 27);
            this.ventana.Margin = new System.Windows.Forms.Padding(4);
            this.ventana.Name = "ventana";
            this.ventana.Size = new System.Drawing.Size(809, 443);
            this.ventana.TabIndex = 1;
            this.ventana.Text = "";
            this.ventana.TextChanged += new System.EventHandler(this.ventana_TextChanged);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Location = new System.Drawing.Point(36, 92);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(156, 43);
            this.button2.TabIndex = 2;
            this.button2.Text = "Caracteristicas de la CPU";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.boton_cpu_Click);
            // 
            // boton_RAM
            // 
            this.boton_RAM.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.boton_RAM.Location = new System.Drawing.Point(36, 158);
            this.boton_RAM.Margin = new System.Windows.Forms.Padding(4);
            this.boton_RAM.Name = "boton_RAM";
            this.boton_RAM.Size = new System.Drawing.Size(156, 43);
            this.boton_RAM.TabIndex = 3;
            this.boton_RAM.Text = "Caracteristicas de la RAM";
            this.boton_RAM.UseVisualStyleBackColor = false;
            this.boton_RAM.Click += new System.EventHandler(this.boton_RAM_Click);
            // 
            // boton_disco
            // 
            this.boton_disco.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.boton_disco.Location = new System.Drawing.Point(36, 224);
            this.boton_disco.Margin = new System.Windows.Forms.Padding(4);
            this.boton_disco.Name = "boton_disco";
            this.boton_disco.Size = new System.Drawing.Size(156, 43);
            this.boton_disco.TabIndex = 4;
            this.boton_disco.Text = "Caracteristicas del Disco duro";
            this.boton_disco.UseVisualStyleBackColor = false;
            this.boton_disco.Click += new System.EventHandler(this.boton_disco_Click);
            // 
            // boton_GPU
            // 
            this.boton_GPU.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.boton_GPU.Location = new System.Drawing.Point(36, 295);
            this.boton_GPU.Margin = new System.Windows.Forms.Padding(4);
            this.boton_GPU.Name = "boton_GPU";
            this.boton_GPU.Size = new System.Drawing.Size(156, 43);
            this.boton_GPU.TabIndex = 5;
            this.boton_GPU.Text = "Caracteristicas de la GPU";
            this.boton_GPU.UseVisualStyleBackColor = false;
            this.boton_GPU.Click += new System.EventHandler(this.boton_GPU_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(856, 487);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(156, 43);
            this.button1.TabIndex = 6;
            this.button1.Text = "Limpiar ventana";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.boton_GPU);
            this.Controls.Add(this.boton_disco);
            this.Controls.Add(this.boton_RAM);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.ventana);
            this.Controls.Add(this.boton_sistema);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button boton_sistema;
        private System.Windows.Forms.RichTextBox ventana;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button boton_RAM;
        private System.Windows.Forms.Button boton_disco;
        private System.Windows.Forms.Button boton_GPU;
        private System.Windows.Forms.Button button1;
    }
}

