﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Management;

namespace WindowsFormsApp7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
        //funcion cuadro de texto
        private void ventana_TextChanged(object sender, EventArgs e)
        {

        }

        private void boton_sistema_Click(object sender, EventArgs e)
        {
            String path = "SYSTEM\\CurrentControlSet\\Control\\ComputerName\\ComputerName";
            RegistryKey key = Registry.LocalMachine.OpenSubKey(path);
            String computerName = key.GetValue("ComputerName").ToString();

			path = "HARDWARE\\DESCRIPTION\\System\\BIOS";
			key = Registry.LocalMachine.OpenSubKey(path);
			String product = key.GetValue("SystemProductName").ToString();

			ventana.SelectionColor = Color.Red;
            ventana.AppendText("Nombre del PC: ");
			ventana.SelectionColor = Color.Black;
			ventana.AppendText(computerName + "\n");

			ventana.SelectionColor = Color.Red;
			ventana.AppendText("Tipo de PC: ");
			ventana.SelectionColor = Color.Black;
            ventana.AppendText(product+"\n");

        }

        private void boton_cpu_Click(object sender, EventArgs e)
        {
            String path = "HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0";
            RegistryKey key = Registry.LocalMachine.OpenSubKey(path);
			String processor = key.GetValue("ProcessorNameString").ToString();
			String id = key.GetValue("Identifier").ToString();

            ventana.SelectionColor = Color.Red;
            ventana.AppendText("Procesador: ");
            ventana.SelectionColor = Color.Black;
			ventana.AppendText(processor + "\n");

			ventana.SelectionColor = Color.Blue;
			ventana.AppendText("	Identificador: ");
			ventana.SelectionColor = Color.Black;
			ventana.AppendText(id + "\n");

		}

        private void boton_GPU_Click(object sender, EventArgs e)
        {
            ManagementObjectSearcher searcher= new ManagementObjectSearcher("SELECT * FROM Win32_DisplayConfiguration");

            string graphicsCard = string.Empty;
            string Hz = string.Empty;
            string width = string.Empty;
            string height = string.Empty;
            foreach (ManagementObject mo in searcher.Get())
            {
                foreach (PropertyData property in mo.Properties)
                {
                    if (property.Name == "Description")
                    {
                        graphicsCard = property.Value.ToString();
                    }else if(property.Name == "DisplayFrequency")
                    {
                        Hz= property.Value.ToString();
                    }else if(property.Name == "PelsHeight")
                    {
                        height = property.Value.ToString();
                    }else if(property.Name ==  "PelsWidth")
                    {
                        width = property.Value.ToString();
                    }
                }
            }

            ventana.SelectionColor = Color.Red;
            ventana.AppendText("Tarjeta de vídeo: ");
            ventana.SelectionColor = Color.Black;
            ventana.AppendText(graphicsCard + "\n");
            ventana.SelectionColor = Color.Red;
            ventana.AppendText("Resolución: ");
            ventana.SelectionColor = Color.Black;
            ventana.AppendText(width + "x" + height + "\n");
            ventana.SelectionColor = Color.Red;
            ventana.AppendText("Herzios de la pantalla: ");
            ventana.SelectionColor = Color.Black;
            ventana.AppendText(Hz + " Hz\n");
        }
        private void boton_RAM_Click(object sender, EventArgs e)
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem");

            string EspacioTotal = string.Empty;
            string EspacioFisicoTotal = string.Empty;
            string EspacioVitualTotal = string.Empty;
            string EspacioLibreF = string.Empty;
            string EspacioLibreV = string.Empty;
            
            foreach (ManagementObject mo in searcher.Get())
            {
                foreach (PropertyData property in mo.Properties)
                {
                    if (property.Name == "SizeStoredInPagingFiles")
                    {
                           EspacioTotal = property.Value.ToString();
                    }
                    else if (property.Name == "TotalVisibleMemorySize")
                    {
                        EspacioFisicoTotal = property.Value.ToString();
                    }
                    else if (property.Name == "TotalVirtualMemorySize")
                    {
                        EspacioVitualTotal = property.Value.ToString();
                    }
                    else if (property.Name == "FreePhysicalMemory")
                    {
                        EspacioLibreF = property.Value.ToString();
                    }
                    else if (property.Name == "FreeVirtualMemory")
                    {
                        EspacioLibreV = property.Value.ToString();
                    }
                }
            }

            ventana.SelectionColor = Color.Red;
            ventana.AppendText("Espacio Total: ");
            ventana.SelectionColor = Color.Black;
            ventana.AppendText(EspacioTotal + " Bytes\n");
            ventana.SelectionColor = Color.Red;
            ventana.AppendText("Espacio Fisico Total: ");
            ventana.SelectionColor = Color.Black;
            ventana.AppendText(EspacioFisicoTotal + " Bytes\n");
            ventana.SelectionColor = Color.Red;
            ventana.AppendText("Espacio VitualTotal: ");
            ventana.SelectionColor = Color.Black;
            ventana.AppendText(EspacioVitualTotal + " Bytes\n");
            ventana.SelectionColor = Color.Red;
            ventana.AppendText("Espacio Libre Fisico: ");
            ventana.SelectionColor = Color.Black;
            ventana.AppendText(EspacioLibreF + " Bytes\n");
            ventana.SelectionColor = Color.Red;
            ventana.AppendText("Espacio Libre vitual: ");
            ventana.SelectionColor = Color.Black;
            ventana.AppendText(EspacioLibreV + " Bytes\n");
        }
        private void button1_Click(object sender, EventArgs e)
        {
            ventana.Clear();
        }

        private void boton_disco_Click(object sender, EventArgs e)
        {
            DriveInfo[] allDrives = DriveInfo.GetDrives();

            foreach (DriveInfo d in allDrives)
            {
                ventana.SelectionColor = Color.Red;
                ventana.AppendText("Disco " + d.Name + "\n");
                ventana.SelectionColor = Color.Blue;
                ventana.AppendText("  Tipo de disco: ");
                ventana.SelectionColor = Color.Black;
                ventana.AppendText(d.DriveType + "\n");
                if (d.IsReady == true)
                {
                    ventana.SelectionColor = Color.Blue;
                    ventana.AppendText("  Etiqueta de volumen: ");
                    ventana.SelectionColor = Color.Black;
                    ventana.AppendText(d.VolumeLabel + "\n");
                    ventana.SelectionColor = Color.Blue;
                    ventana.AppendText("  Sistema de archivos: ");
                    ventana.SelectionColor = Color.Black;
                    ventana.AppendText(d.DriveFormat + "\n");
                    ventana.SelectionColor = Color.Blue;
                    ventana.AppendText(
                        "  Espacio libre para el usuario actual ");
                    ventana.SelectionColor = Color.Black;
                    ventana.AppendText(d.AvailableFreeSpace / 1024 / 1024 / 1024 + " GB\n");
                    ventana.SelectionColor = Color.Blue;
                    ventana.AppendText(
                        "  Espacio libre total: ");
                    ventana.SelectionColor = Color.Black;
                    ventana.AppendText(d.TotalFreeSpace/1024/1024/1024 + " GB\n");
                    ventana.SelectionColor = Color.Blue;
                    ventana.AppendText(
                        "  Tamaño total del disco: ");
                    ventana.SelectionColor = Color.Black;
                    ventana.AppendText(d.TotalSize / 1024 / 1024 / 1024 + " GB\n");
                }
            }
        }
    }
}
